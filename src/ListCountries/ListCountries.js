import React from 'react';
import CountriesService from '../services/countries.service';
import TrCountry from '../TrCountry/TrCountry';
import CountryForm from '../CountryForm/CountryForm';

class ListCountries extends React.Component {

    constructor(props) {
        super(props);

        this.countriesService = new CountriesService();

        this.state = {
            countries: []
        };
    }

    removeCountry = index => {
        const { countries } = this.state; // équivalent à : const countries = this.state.countries;
        
        this.setState({
            countries: countries.filter((country, i) => {
                return i !== index
            })
        });
    };

    componentDidMount() {
        this.countriesService.getCountries()
            .then(countries => this.setState({
                countries
            }));
    }

    handleAdd = country => {
        this.setState({
            countries: [...this.state.countries, country] // Immuabilité : on ne modifie jamais les données directement, on doit passer par setState et fournir des données neuves
        });
    };

    render() {
        const { countries } = this.state;

        const countriesItems = countries.map((country, index) => 
            <TrCountry key={index} country={country} index={index} removeCountry={this.removeCountry} />
        );

        return (
            <div className="ListCountries">
                <section className="ui vertical secondary padded segment">
                    <article className="ui container">
                        <CountryForm handleAdd={this.handleAdd} />
                    </article>
                </section>
                <section className="ui vertical padded segment">
                    <div className="ui container">
                        <table className="ui table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Capital</th>
                                    <th>Remove</th>
                                </tr>
                            </thead>
                            <tbody>
                                {countriesItems}
                            </tbody>
                        </table>
                    </div>
                </section>
            </div>
        );
    }
}

export default ListCountries;