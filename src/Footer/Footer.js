import React from 'react';

function Footer() {
    return (
        <footer className="Footer">
            <div className="ui container">
                <p>© Super App - This is the footer</p>
            </div>
        </footer>
    );
}

export default Footer;