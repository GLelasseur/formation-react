class CountriesService {
    constructor() {
    }

    getCountries() {
        const apiURL = 'https://restcountries.eu/rest/v2/all';

        return fetch(apiURL)
                .then(data => data.json());
    }
}

export default CountriesService;