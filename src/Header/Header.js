import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function Header(props) {
    return (
        <header className="Header">
            <nav className="ui menu">
                <div className="ui container">
                    <h1 className="header item">{props.appDetails.name}</h1>
                    <Link to="/" className="item">Home</Link> 
                    <Link to="/countries" className="item">Countries</Link>
                </div>
            </nav>
        </header>
    );
}

export default Header;