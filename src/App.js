import React from 'react';
import './App.css';

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Header from './Header/Header';
import Footer from './Footer/Footer';
import ListCountries from './ListCountries/ListCountries';
import Index from './Index/Index';

function App() {
  const appDetails = {
    name: "Super App"
  };
  return (
    <Router>
      <div className="App">
        <Header appDetails={appDetails} />
        <main>
          <Route path="/" exact component={Index} />
          <Route path="/countries" exact component={ListCountries} />
        </main>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
