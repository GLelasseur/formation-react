import React from 'react';

class TrCountry extends React.Component {
    render() {
        return (
            <tr>
                <td>{this.props.country.name}</td>
                <td>{this.props.country.capital}</td>
                <td>
                    <button 
                        onClick={() => this.props.removeCountry(this.props.index)} 
                        className="ui negative button">
                        <i className="trash icon"></i>
                        Remove
                    </button>
                </td>
            </tr>
        );
    };
}

export default TrCountry;