import React from 'react';

class CountryForm extends React.Component {

    constructor(props) {
        super(props);

        this.initialState = {
            name: '',
            capital: ''
        };

        this.state = this.initialState;
    }

    handleChange = event => {
        const { name, value } = event.target;

        this.setState({
            [name]: value // Les crochets permettent d’assigner la valeur de la variable name
        });
    };

    handleSubmit = event => {
        event.preventDefault(); // Empêcher l’envoi par défaut du formulaire

        this.props.handleAdd(this.state); // On ajoute le pays au state du composant parent (ListCountries)

        this.setState(this.initialState); // On remet le formulaire à zéro
    }

    render() {
        const { name, capital } = this.state;

        return (
            <form className="ui form" onSubmit={this.handleSubmit}>
                <h3>Add a country</h3>
                <p>
                    <label>
                        Name:
                        <input
                            type="text"
                            name="name"
                            value={name}
                            onChange={this.handleChange}
                            />
                    </label>
                </p>
                <p>
                    <label>
                        Capital:
                        <input
                            type="text"
                            name="capital"
                            value={capital}
                            onChange={this.handleChange}
                            />
                    </label>
                </p>
                <p>
                    <input type="submit" value="Add" className="ui primary button"/>
                </p>
            </form>
        );
    }
}

export default CountryForm;