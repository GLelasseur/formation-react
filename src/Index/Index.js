import React from 'react';
import ListCountries from '../ListCountries/ListCountries';

class Index extends React.Component {
    render() {
        return (
            <section className="ui vertical inverted padded segment">
                <h2 className="ui header">
                    <div className="ui container">
                        Welcome on the world countries app
                    </div>
                </h2>
            </section>
        );
    }
}

export default Index;